#!/usr/bin/env ruby
#
# Measures HTTP response times over 5 minutes to https://gitlab.com
#

require 'faraday'
require 'faraday_middleware'
require 'benchmark'
require 'descriptive-statistics'
require 'pry'

def measure(connection)
  failure = false
  result = Benchmark.measure do
    begin
      connection.get do |t|
        t.url '/'
        t.options.timeout = 5
        t.options.open_timeout = 5
      end
    rescue Faraday::ConnectionFailed, Faraday::TimeoutError
      fail = true
      nil
    end
  end.real
  result unless failure
end

def report(responses)
  stats = DescriptiveStatistics::Stats.new(responses)
  puts "Mean: #{stats.mean}"
  puts "Median: #{stats.median}"
  puts "Mode: #{stats.mode}"
end

future = Time.new + 5*60 # 5 minutes in the future, when we're going to stop

responses = Array.new

# print stats even if we exit early
trap 'SIGINT' do
  report(responses)
  exit 130
end

url = 'https://gitlab.com'

conn = Faraday.new(url) do |r|
    r.use FaradayMiddleware::FollowRedirects
    r.adapter :net_http
end

puts 'Starting up and measuring; this may take awhile (or hit ctrl-c to see as many stats as are available'

while Time.new < future
  responses << measure(conn)
end

report(responses.compact)
