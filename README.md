# Sample app - tracking response times over 5 minutes from GitLab

This is a traditional Ruby setup, so to run it, make sure you have the Bundler gem installed. Then, run `bundle` and ultimately `bundle exec ./measure.rb`!

# REQS

- A supported Ruby version; I use 2.4.1 on rbenv
- Bundler

# NOTES

This is a fairly straightforward way of solving this problem; the flow looks like

 - Set up an HTTP connection to gitlab
 - Until we're five minutes into the future,
   - Time how long it takes to get a response back (measured in 'real' wall-clock time)
   - Add that data point to an array of these results
 - When five minutes elapse- or a user hits control-C- print out some basic statistics about the data set generated

# CAVEATS

This doesn't handle every 'not great' case that could turn up- for example, the request may time out entirely! Results like that are tossed aside entirely

Additionally, requests are not rate-limited and happen in serial.
